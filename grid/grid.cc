#include <cstdlib>
#include "./grid.h"

GridCell* gridcell_new(const float x, const float y, const float w, const float h) {
	/*
	 * Creates a new GridCell to insert into a Grid
	 */
	GridCell* cell = (GridCell*)malloc(sizeof(GridCell));

	cell->active = false;

	cell->x = x;
	cell->y = y;

	cell->width = w;
	cell->height = h;

	return cell;
}

Grid* grid_new(const int w, const int h) {
	/*
	 * Creates a new grid for use in partitioning schemes
	 */
	Grid* grid = (Grid*)malloc(sizeof(Grid));

	grid->width = w;
	grid->height = h;
	
	grid->cells = NULL;
	grid->cell_count = 0;

	return grid;
}

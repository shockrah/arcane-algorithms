#ifndef GRID_H

#define GRID_H
#define GRID_CELL_MAX_MEMBERS 1000

struct GridCell {
	// Mostly used for debugging purposes but can be interesting to see what
	// cells have some members
	bool active;
	// Graphically at the top left corner of the cell
	float x, y;
	// Tells us how wide and tall the cell is (graphically)
	float width, height;
};
GridCell* gridcell_new(const float x, const float y, const float w, const float h);

struct Grid {
	int width;
	int height;

	GridCell* cells;
	int cell_count;

};

Grid* grid_new(const int w, const int h);
#endif

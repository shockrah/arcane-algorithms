cc=g++ -Wall -O2 -Werror
libs=-lm -lSDL2
bin=bin

modules := canvas engine rain boids input grid
build_objects := $(foreach m,$(modules),$(bin)/$(m).o)

sim: $(build_objects)
	$(cc) $(build_objects) -o $(bin)/sim $(libs)



$(bin)/canvas.o: canvas/canvas.cc
	$(cc) -c $< -o $@ $(libs)

$(bin)/grid.o: grid/grid.cc
	$(cc) -c $< -o $@ $(libs)

$(bin)/engine.o: engine/engine.cc
	$(cc) -c $< -o $@ $(libs)

$(bin)/rain.o: rain/rain.cc
	$(cc) -c $< -o $@ $(libs)

$(bin)/boids.o: boids/boids.cc
	$(cc) -c $< -o $@ $(libs)

$(bin)/input.o: input/input.cc
	$(cc) -c $< -o $@ $(libs)

run:
	$(bin)/sim

.PHONY: clean sim run

clean:
	rm -f $(bin)/*

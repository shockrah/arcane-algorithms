#include "input.h"
bool q_pressed(SDL_Event* e) {
	SDL_PollEvent(e);

	// Early quit in case the event is somehow busted
	if(e->type == SDL_KEYDOWN) {
		return e->key.keysym.sym == SDLK_q;
	} else {
		return e->type == SDL_QUIT;
	}
}


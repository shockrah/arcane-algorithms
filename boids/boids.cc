#include <SDL2/SDL.h>
#include <ctime>
#include <cmath>
#include <iostream>
#include "./boids.h"
#include "../canvas/canvas.h"
#include "../input/input.h"

// just over actual pi because we want to include the full range
#define PI 3.15

void boid_draw(const Boid* boid) {
	// For now we're just going to draw a square instead of anything else
	SDL_Rect r = { (int)boid->x, (int)boid->y, 5, 5 };
	SDL_RenderFillRect(Canvas::render, &r);
}


Boid* boid_new(const int max_x, const int max_y) {
	Boid* boid = (Boid*)malloc(sizeof(Boid));
	boid->x = rand() % max_x;
	boid->y = rand() % max_y;
	boid->speed = 4;
	boid->direction = 0;
	boid->direction = ((float)rand()) / ((float)(RAND_MAX)) * (2.0 * PI);
	return boid;
}

void update_boids(const int count, Boid** flock) {
	for(int i = 0; i < count; i++) {
		// Move the boids in the right direction
		const float x_offset = flock[i]->speed * cos(flock[i]->direction);
		const float y_offset = flock[i]->speed * sin(flock[i]->direction);

		flock[i]->x += x_offset;
		flock[i]->y += y_offset;

		// Move to the bottom of the screen
		if(flock[i]->x < 0) { flock[i]->x = Canvas::width(); }
		if(flock[i]->y < 0) { flock[i]->y = Canvas::height(); }

		if (flock[i]->x > Canvas::width()) { flock[i]->x = 0; }
		if(flock[i]->y > Canvas::height()) { flock[i]->y = 0; }

	}
}

void draw_boids(const int count, Boid** flock) {
	SDL_SetRenderDrawColor(Canvas::render, 0, 0x7d, 0x8c, 0xff);
	for(int i = 0; i < count; i++) {
		boid_draw(flock[i]);
	}
}

void boid_sim(void) {
	const int FLOCK_SIZE = 100;
	Boid* flock[FLOCK_SIZE];
	std::srand(std::time(nullptr));

	std::cout << "Canvas: " << Canvas::width() << Canvas::height() << "\n";
	for(int i = 0; i < FLOCK_SIZE; i++) {
		flock[i] = boid_new(Canvas::width(), Canvas::height());
		std::cout << flock[i]->x << " " << flock[i]->y << "\n";
	}


	SDL_Event event;
	while(q_pressed(&event) == false) {
		Canvas::clear(0x19, 0x19, 0x19);
		update_boids(FLOCK_SIZE, flock);
		draw_boids(FLOCK_SIZE, flock);
		Canvas::update(10);
	}
}

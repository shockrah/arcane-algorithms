#ifndef BOI_H
#define BOI_H

struct Boid {
	float x; float y;
	unsigned int speed;
	float direction; // radians

	void draw(void);
};

Boid* boid_new(const int max_x, const int max_y);
void boid_draw(const Boid* boid);
void boid_sim(void);

#endif

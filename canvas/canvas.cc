#include "./canvas.h"
#include "SDL2/SDL.h"

#define WIN_TITLE "Engine"


namespace Canvas {
	SDL_Window* window = NULL;
	SDL_Renderer* render = NULL;
	int init(void) {
		if(SDL_Init(SDL_INIT_VIDEO) != 0) {
			SDL_Log("Failed to init video: %s", SDL_GetError());
			return CANVAS_VID_INIT_ERR;
		}

		window = SDL_CreateWindow(
			WIN_TITLE,
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			WIN_WIDTH,
			WIN_HEIGHT,
			SDL_WINDOW_SHOWN
		);
		if(!window) {
			SDL_Log("failed to create window: %s", SDL_GetError());
			return CANVAS_WINDOW_CREATE_ERR;
		}

		render = SDL_CreateRenderer(
			window,
			-1/* let sdl figure out the right video driver */,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
		);
		if(!render) {
			SDL_DestroyWindow(window);
			SDL_Log("Renderer could not be created: %s\n", SDL_GetError());
			SDL_Quit();
			return CANVAS_RENDER_CREATE_ERR;
		}
		SDL_SetRenderDrawColor(render, 0xff, 0xff, 0xff, 0xff);
		return CANVAS_INIT_SUCCESS;
	}


	void close(void) {
		// Creates the SDL_Quit event
		SDL_Quit();
	}

	void update(int delay/* delay in ms*/) {
		//SDL_UpdateWindowSurface(window);
		SDL_Delay(delay);
		SDL_RenderPresent(render);
	}

	void clear(uint8_t r, uint8_t g, uint8_t b) {
		// Clears the window to be all black
		SDL_SetRenderDrawColor(render, r, g, b, 0xff);
		SDL_RenderClear(render);
	}

	void cleanup(void) {
		SDL_DestroyRenderer(render);
		SDL_DestroyWindow(window);
	}

	int width(void) {
		return WIN_WIDTH;
	}

	int height(void) {
		return WIN_HEIGHT;
	}

}

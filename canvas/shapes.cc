#include <SDL2/SDL.h>
#include <math.h>
#include <vector>
#include "../canvas/canvas.h"
#include "shapes.h"

void Point::draw() {
	SDL_RenderDrawPoint(Canvas::render, this->x, this->y);
}

inline float _rotate_x(float theta, float x, float y) {
	if(theta  == 0.0) { return x; }
	return (x * cos(theta)) - (y * sin(theta));
}
inline float _rotate_y(float theta, float x, float y) {
	if(theta  == 0.0) { return y; }
	return (x * sin(theta)) + (y * cos(theta));
}

void Rect::draw() {
	/*
	 * First start at the bottom left  corner then
	 */
	/* Anchor point/bottom left is this->(x,y) */
	/* Bottom right (x,y) */
	float brx = this->x + this->base; float bry = this->y;
	float r_brx = _rotate_x(this->theta,  brx,  bry);
	float r_bry = _rotate_y(this->theta,  brx,  bry);
	/* Top left (x,y) */
	float tlx = this->x; float tly = this->y + this->height;
	float r_tlx = _rotate_x(this->theta,  tlx,  tly);
	float r_tly = _rotate_y(this->theta,  tlx,  tly);
	/* Top right (x,y) */
	float trx = this->x + this->base; float try_ = this->y + this->height;
	float r_trx = _rotate_x(this->theta,  trx,  try_);
	float r_try_ = _rotate_y(this->theta,  trx,  try_);

	// For now we don't bother shading in this area  we just do the outlines
	/* rp -> br -> tr -> tl */
	SDL_RenderDrawLine(Canvas::render, this->x, this->y, r_brx,  r_bry);
	SDL_RenderDrawLine(Canvas::render, r_brx, r_bry,  r_trx, r_try_);
	SDL_RenderDrawLine(Canvas::render, r_trx, r_try_, r_tlx, r_tly);
	SDL_RenderDrawLine(Canvas::render, r_tlx, r_tly, this->x, this->y);


	SDL_Rect rect = {this->x, this->x, (int)this->base, (int)this->height};
	SDL_RenderFillRect(Canvas::render, &rect);
}


void Line::draw(void) {
	SDL_RenderDrawLine(Canvas::render, this->xs, this->ys, this->xe, this->ye);
}

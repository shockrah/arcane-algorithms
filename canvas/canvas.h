#ifndef CANVAS_H
#define CANVAS_H
#include "SDL2/SDL.h"

#define WIN_WIDTH 640
#define WIN_HEIGHT 480

#define CANVAS_INIT_SUCCESS 0
#define CANVAS_VID_INIT_ERR 1
#define CANVAS_WINDOW_CREATE_ERR 2
#define CANVAS_RENDER_CREATE_ERR 3

namespace Canvas {
	extern SDL_Window* window;
	extern SDL_Renderer* render;

    int init(void);

    void paint_window(const int width, const int height);

    void paint_square(const int, const int, const int);

    void close(void);

    void update(int delay/*ms*/);

    void clear(uint8_t, uint8_t, uint8_t);

    void cleanup(void);

	int width(void);

	int height(void);
}
#endif // CANVAS_H

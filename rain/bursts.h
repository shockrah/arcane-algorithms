/*
 * Bursts are going to just be circles that keep growing per frame
 * from their initial size to their final size
 * They also slowly lower until they are offscreen and we can safely not
 * draw them anymore.
 * */
struct Burst {
	int x;
	int y;
	int radius;
	int max_radius;
};

void burst_draw(Burst* burst);

void burst_add(const int x, const int radius);

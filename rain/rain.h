#ifndef RAIN_H
#define RAIN_H

#define DROP_COUNT 100

struct RainDrop {
	int x; int y; int z;
	int width; int height;
	int vel;
};


RainDrop* raindrop_new(const int upperx, const int uppery);
void raindrop_draw(const RainDrop* self);
void rain_sim(void);

#endif

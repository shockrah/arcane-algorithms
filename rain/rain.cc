#include <SDL2/SDL.h>
#include <cstdlib>
#include <ctime>

#include "./rain.h"
#include "../canvas/canvas.h"
#include "../input/input.h"


using std::rand;

RainDrop* raindrop_new(const int upperx, const int uppery) {
	RainDrop* drop = (RainDrop*)malloc(sizeof(RainDrop));
	// cpost
	drop->x = rand() % upperx;
	drop->y = rand() % uppery;

	// sizing
	drop->width = (rand() % 4) + 1;
	drop->height = (rand() % 24) + 1;

	// vel + layering
	drop->vel = (rand() % 14) + 4; // minimum speed of 4 with a max of 
	drop->z = rand() % 15;
	return drop;
}

void raindrop_draw(const RainDrop* self) {
	/*
	 * This draws the rain drop body itself and also determines if the burst
	 */
	SDL_Rect r = { self->x, self->y, self->width, self->height };
	SDL_SetRenderDrawColor(Canvas::render, 0, 0x7d, 0x8c, 0xff);
	SDL_RenderFillRect(Canvas::render, &r);
}


void rain_sim(void) {
	// Create our collection of rain drops
	RainDrop* drops[DROP_COUNT];
	// Seed with current time to make every drop have a new random location
	std::srand(std::time(nullptr));
	for(int i = 0; i < DROP_COUNT; i++) {
		drops[i] = raindrop_new(Canvas::width(), Canvas::height());
	}

	SDL_Event event;
	while(q_pressed(&event) == false) {
		Canvas::clear(0x19, 0x19, 0x19);

		// Primary rain logic
		SDL_SetRenderDrawColor(Canvas::render, 0xff, 0, 0, 0xff);
		// Take the current rain drop and drop its position by some amount
		for(int i = 0; i < DROP_COUNT; i++) {
			if(drops[i]->y > Canvas::height()) {
			}

			drops[i]->y += drops[i]->vel; // Adjust the y pos of the rain drop
			drops[i]->y %= Canvas::height(); // Clamp the position to the windows
			raindrop_draw(drops[i]);
		}

		Canvas::update(10);
	}
	Canvas::cleanup();
	for(int i = 0; i< DROP_COUNT; i++) {
		free(drops[i]);
	}
}

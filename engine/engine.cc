// This main module is basically just used to choose what simulation we
// want to run, the first sim that we're going to build out is a simple
// rain simulation because why tf not
#include <iostream>
#include <SDL2/SDL.h>

#include "../canvas/canvas.h"
#include "../rain/rain.h"
#include "../boids/boids.h"


int main(void) {
	if(Canvas::init() != CANVAS_INIT_SUCCESS) {
		std::cerr << "Error initializing canvas\n";
		return 1;
	}

	boid_sim();
	//rain_sim();
	Canvas::cleanup();
	return 0;
}
